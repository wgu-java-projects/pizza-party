package com.zybooks.pizzaparty;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivityController extends AppCompatActivity {

    public final int SLICES_PER_PIZZA = 8;

    private EditText mNumAttendEditText;
    private TextView mNumPizzasTextView;
    private RadioGroup mHowHungryRadioGroup;
    private int mTotalPizzas;
    private static String KEY_TOTAL_PIZZAS = "totalPizzas";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assign the widgets to fields
        mNumAttendEditText = findViewById(R.id.attendEditText);
        mNumPizzasTextView = findViewById(R.id.answerTextView);
        mHowHungryRadioGroup = findViewById(R.id.hungryRadioGroup);

        if (savedInstanceState != null) {
            mTotalPizzas = savedInstanceState.getInt(KEY_TOTAL_PIZZAS);
            displayTotal();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(KEY_TOTAL_PIZZAS, mTotalPizzas);
    }

    public void calculateClick(View view) {
        // Get how many are attending the party
        int numAttend;
        try {
            String numAttendStr = mNumAttendEditText.getText().toString();
            numAttend = Integer.parseInt(numAttendStr);
        } catch (NumberFormatException ex) {
            numAttend = 0;
        }

        // Get hunger level selection
        int checkedId = mHowHungryRadioGroup.getCheckedRadioButtonId();
        PizzaCalculatorModel.HungerLevel hungerLevel = PizzaCalculatorModel.HungerLevel.RAVENOUS;
        if (checkedId == R.id.lightRadioButton) {
            hungerLevel = PizzaCalculatorModel.HungerLevel.LIGHT;
        } else if (checkedId == R.id.mediumRadioButton) {
            hungerLevel = PizzaCalculatorModel.HungerLevel.MEDIUM;
        }

        // Get the number of pizzas needed
        PizzaCalculatorModel calc = new PizzaCalculatorModel(numAttend, hungerLevel);
        mTotalPizzas = calc.getTotalPizzas();

        // Place totalPizzas into the string resource and display
        displayTotal();
    }

    private void displayTotal() {
        String totalText = getString(R.string.total_pizzas, mTotalPizzas);
        mNumPizzasTextView.setText(totalText);
    }
}